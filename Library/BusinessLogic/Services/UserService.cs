﻿using AutoMapper;
using Library.BusinessLogic.DTOs;
using Library.DataAccess;
using Library.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace Library.Services
{
	public class UserService:ICrudService<UserDTO>
	{
		private readonly BinaryDataContext _context;
		private readonly IMapper _mapper;

		public UserService(BinaryDataContext source, IMapper mapper)
		{
			_context = source;
			_mapper = mapper;
		}

		public async Task CreateAsync(UserDTO entity)
		{
			//user has Team FK
			if (_context.Teams.SingleOrDefault(t => t.Id == entity.Team_Id) == null)
				throw new Exception("Invalid team Id");

			await _context.Users.AddAsync(_mapper.Map<User>(entity));

			await _context.SaveChangesAsync();
		}

		public async Task DeleteAsync(int id)
		{
			var user = await _context.Users.SingleOrDefaultAsync(u => u.Id == id);
			_context.Users.Remove(user);

			await _context.SaveChangesAsync();
		}

		public async Task<UserDTO> ShowAsync(int Id)
		{
			var user = await _context.Users.SingleOrDefaultAsync(u => u.Id == Id);
			return _mapper.Map<UserDTO>(user);
		}

		public async Task<List<UserDTO>> ShowAllAsync()
		{
			var users = await _context.Users.ToListAsync();
			return _mapper.Map<List<UserDTO>>(users);
		}

		public async Task UpdateAsync(UserDTO entity)
		{
			//user has Team FK
			if (_context.Teams.SingleOrDefault(t => t.Id == entity.Team_Id) == null)
				throw new Exception("Invalid team Id");

			var user = await _context.Users.SingleOrDefaultAsync(u => u.Id == entity.Id);//find this user in collection
			user = _mapper.Map<User>(entity);

			_context.Entry(user).State = EntityState.Modified;

			await _context.SaveChangesAsync();
		}

		public async Task<bool> IsExistsAsync(int id)
		{
			return await  _context.Users.SingleOrDefaultAsync(u => u.Id == id) == null ? false : true;
		}
	}
}
