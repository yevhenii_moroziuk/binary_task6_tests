﻿using Client.Helpers;
using Client.Models;
using Client.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Client
{
	public class Program
	{
		private static List<Models.Task> queries;
		private static System.Timers.Timer timer;
		public static async System.Threading.Tasks.Task Main(string[] args)
		{
			//IServiceCollection services;
			//IServiceProvider serviceProvider;
			IConfiguration configuration;
			//DataService dataService;
			//ClientHub hub;
			bool IsFinished = false;

			configuration = new ConfigurationBuilder()
			.SetBasePath(Directory.GetCurrentDirectory())
			.AddJsonFile("appsettings.json")
			.Build();

			var services = new ServiceCollection();
			services.AddSingleton<IDataClient>(new DataClient(configuration));
			var serviceProvider = services.BuildServiceProvider();

			var dataService = new DataService(serviceProvider.GetService<IDataClient>());
			var hub = new ClientHub(configuration);

			Helper.ShowGeneralInfo();

			await hub.WorkAsync();

			while (!IsFinished)
			{
				switch (Helper.InputInteger(1, 10))
				{
					case 1:
						await dataService.Task1Async(50);
						break;
					case 2:
						await dataService.Task2Async(30);
						break;
					case 3:
						await dataService.Task3Async(50);
						break;
					case 4:
						await dataService.Task4Async();
						break;
					case 5:
						await dataService.Task5Async();
						break;
					case 6:
						await dataService.Task6Async(50);
						break;
					case 7:
						await dataService.Task7Async(7);
						break;
					case 8:
						await dataService.Task8Async();
						break;
					case 9:
						timer = new System.Timers.Timer();
						timer.Interval = 1000;
						timer.Elapsed += async (o, e) =>
						{
							Random random = new Random();
							queries = await dataService.GetTasksList();
							var task = queries[random.Next(queries.Count())];

							task.State = TaskState.Finished;

							string output = JsonConvert.SerializeObject(task);

							var ans = await HttpPut(output);

							if (ans.Contains("edit"))
								Console.WriteLine($"task {task.Id} is finished");
							else
								throw new Exception("Can`t mark task as finished");

							timer.Dispose();
						};
						timer.AutoReset = false;
						timer.Enabled = true;
						break;						
					case 10:
						IsFinished = true;
						break;
					default:
						await hub.StopAsync();
						break;
				}
			}
			Console.ReadKey();
		}
			public static async Task<string> HttpPut(string obj)
			{
				using (var client = new HttpClient())
				{

					HttpRequestMessage request = new HttpRequestMessage()
					{

						Content = new StringContent(obj,
						Encoding.UTF8, "application/json"),
						Method = HttpMethod.Put,
						RequestUri = new Uri("https://localhost:44305/api/tasks")
					};

					using (HttpResponseMessage resp = await client.SendAsync(request))
							{
								resp.EnsureSuccessStatusCode();

								return await resp.Content.ReadAsStringAsync();
							}
						}
					}
	}
			
	public static class Extantion
	{
		public static Task<int> MarkRandomTaskWithDelay(this IList l)
		{
			var tcs = new TaskCompletionSource<int>();

			var bgWorker = new BackgroundWorker();

			bgWorker.DoWork += (o, e) =>
			 {
				 Random random = new Random();
				 var i = random.Next(l.Count);
				 Client.Models.Task task = l[i] as Client.Models.Task;
				 task.State = Client.Models.TaskState.Finished;
				 string output = JsonConvert.SerializeObject(task);
				 HttpRequestMessage request = new HttpRequestMessage()
				 {

				 	Content = new StringContent(output.ToString(),
				 	Encoding.UTF8, "application/json"),
				 	Method = HttpMethod.Put,
				 	RequestUri = new Uri("https://localhost:44305/api/tasks")
				 };
				 e.Result = task.Id;
			 };

			bgWorker.RunWorkerCompleted += (o, e) =>
			{
				if (e.Error != null)
					tcs.SetException(e.Error);
				else
					tcs.SetResult((int)e.Result);
				Console.WriteLine("ffffff");

			};
			return tcs.Task;
		}

	
}
}