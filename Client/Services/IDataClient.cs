﻿using System.Threading.Tasks;

namespace Client.Services
{
	public interface IDataClient
	{
		Task<T> GetAsync<T>(string specificAddres);

		Task EditAsync<T>(T obj, string specificAddres);
	}
}
