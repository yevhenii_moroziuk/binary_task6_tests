﻿using Client.Helpers;
using Client.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace Client.Services
{
	public class DataService
	{
		private IDataClient _client;

		public DataService(IDataClient client)
		{
			_client = client;
		}

		public async Task Task1Async(int id)
		{
			Helper.TaskDefinition(1);
			var task2 = await _client.GetAsync<Dictionary<string, int>>($"prtask/task1/{id}");
			foreach (var item in task2)
			{
				Console.WriteLine($"{item.Key}   {item.Value}");
			}

			Console.WriteLine("================================");
		}

		public async Task Task2Async(int id)
		{
			Helper.TaskDefinition(2);
			var task1 = await _client.GetAsync<List<Models.Task>>($"prtask/task2/{id}");

			foreach (var item in task1)
			{
				Console.WriteLine($"{item.ToString()}");
			}

			Console.WriteLine("================================");
		}

		public async Task Task3Async(int id)
		{
			Helper.TaskDefinition(3);
			var task3 = await _client.GetAsync<List<string>>($"prtask/task3/{id}");

			foreach (var item in task3)
			{
				Console.WriteLine(item);
			}

			Console.WriteLine("================================");
		}

		public async Task Task4Async()
		{
			Helper.TaskDefinition(4);
			var task4 = await _client.GetAsync<IDictionary<string, List<User>>>("prtask/task4");

			foreach (var item in task4)
			{
				Console.WriteLine(item.Key);
				foreach (var i in item.Value)
				{
					Console.WriteLine(i.First_Name);
				}
			}

			Console.WriteLine("================================");
		}

		public async Task Task5Async()
		{
			Helper.TaskDefinition(5);
			var task5 = await _client.GetAsync<IDictionary<string, List<Models.Task>>>("prtask/task5");

			foreach (var item in task5)
			{
				//Console.WriteLine("User name: " + item.Key.First_Name);
				Console.BackgroundColor = ConsoleColor.DarkGray;
				Console.WriteLine(item.Key);
				Console.ResetColor();
				foreach (var task in item.Value)
				{
					Console.WriteLine("	Task name: " + task.Name);
					//Console.WriteLine(i.ToString());
				}
			}

			Console.WriteLine("================================");
		}

		public async Task Task6Async(int id)
		{
			Helper.TaskDefinition(6);
			var task6 = await _client.GetAsync<Task6>($"prtask/task6/{id}");

			Console.WriteLine("User: " + task6.User.ToString());
			Console.WriteLine("Last project: " + task6.LastProject.ToString());
			Console.WriteLine("Count of all tasks in the last project: " + task6.TaskCount);
			Console.WriteLine("Count of cancled/uncomplited tasks in the last project: " + task6.UnfinishedTaskCount);
			Console.WriteLine("Longest task: " + task6.LongestTask.ToString());

			Console.WriteLine("================================");
		}

		public async Task Task7Async(int id)
		{
			Helper.TaskDefinition(7);
			var task7 = await _client.GetAsync<Task7>($"prtask/task7/{id}");
			Console.WriteLine("Project: " + task7.Project.ToString());
			Console.WriteLine("Longest project`s task by definition: " + task7.TaskByDescription.ToString());
			Console.WriteLine("Shortest project`s task by name: " + task7.TaskByLength.ToString());
			Console.WriteLine("Count of users in current project (0 - if do not satisfy conditions)" + task7.CountOfUsers);

			Console.WriteLine("================================");
		}
		public async Task Task8Async()
		{
			Console.WriteLine("Log");
			var task8 = await _client.GetAsync<List<LogModel>>($"prtask/log");

			foreach (var item in task8)
			{
				Console.WriteLine($"Time: {item.WrittenData}, Message: {item.Message}");
			}
			Console.WriteLine("================================");
		}

		//get unfinished tasks
		public async Task<List<Models.Task>> GetTasksList()
		{
			var tasks = await _client.GetAsync<List<Models.Task>>("tasks");

			return tasks.Where(t => t.State != TaskState.Finished).ToList();
		}

		public async Task Edit(Models.Task task)
		{
			task.State = TaskState.Finished;
			await _client.EditAsync(task, "tasks");
		}
	}
}
