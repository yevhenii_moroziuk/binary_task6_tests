﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Library.BusinessLogic.DTOs;
using Library.Services;
using Microsoft.AspNetCore.Mvc;

namespace Task5.API.Controllers
{
	[Route("api/users")]
	[ApiController]
	public class UserController : ControllerBase
	{
		private readonly ICrudService<UserDTO> _service;

		public UserController(ICrudService<UserDTO> service)
		{
			_service = service;
		}

		[HttpGet]
		public async Task<ActionResult<List<UserDTO>>> GetUsersAsync()
		{
			return await _service.ShowAllAsync();
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> GetUserAsync(int id)
		{
			if (!await _service.IsExistsAsync(id))
				return NotFound();

			return Ok(await _service.ShowAsync(id));
		}

		[HttpPost]
		public async Task<IActionResult> PostUserAsync(UserDTO userDTO)//(UserDTO userDTO)
		{
			if (!ModelState.IsValid)
				return BadRequest();

			if (await _service.IsExistsAsync(userDTO.Id))
				return BadRequest();

			try
			{
				await _service.CreateAsync(userDTO);
				return Created("api/users", userDTO);
			}
			catch
			{
				//throw new Exception("Invalid team Id");
				return BadRequest("Invalid team id");
			}
		}

		[HttpPut]
		public async Task<IActionResult> EditUserAsync(UserDTO entity)
		{
			if (!ModelState.IsValid)
				return BadRequest("Invalid user id");

			if (!await _service.IsExistsAsync(entity.Id))
				return NotFound();

			try
			{
				await _service.UpdateAsync(entity);
				return Ok($"edited: {entity.ToString()}");
			}
			catch
			{
				return BadRequest("error while updating the entity");
			}
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteUserAsync(int id)
		{
			if (!await _service.IsExistsAsync(id))
				return BadRequest();

			await _service.DeleteAsync(id);

			return Ok("deleted");
		}
	}
}