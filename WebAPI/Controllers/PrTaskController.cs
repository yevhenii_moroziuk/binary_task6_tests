﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Library.BusinessLogic.DTOs;
using Library.DataAccess.Models;
using Library.Services;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Queue;

namespace WebAPI.Controllers
{
	[Route("api/[controller]")]
    [ApiController]
    public class PrTaskController : ControllerBase
    {
		private ILinqTaskService _lts;
		private readonly QueueService _service;

		public PrTaskController(ILinqTaskService taskRepository, QueueService service)
		{
			_lts = taskRepository;
			_service = service;
		}

		[HttpGet("task1/{author}")]
		public async Task<IDictionary<string, int>> Task1Async(int author)
		{
			_service.TransferMessage("task1 started");

			return await _lts.GetTasksCountAsync(author);
		}

		[HttpGet("task2/{userid}")]
		public async Task<List<Library.DataAccess.Models.Task>> Task2Async(int userid)
		{
			_service.TransferMessage("task2 started");

			return await _lts.GetTasksAsync(userid);
		}

		[HttpGet("task3/{userid}")]
		public async Task<List<string>> Task3Async(int userid)
		{
			_service.TransferMessage("task3 started");

			return await _lts.GetFinishedTasksAsync(userid);
		}

		[HttpGet("task4/")]
		public async Task<IDictionary<string, List<User>>> Task4Async()
		{
			_service.TransferMessage("task4 started");

			return await _lts.GetTeamDefinitionAsync();
		}

		[HttpGet("task5/")]
		public async Task<IDictionary<User, List<Library.DataAccess.Models.Task>>> Task5Async()
		{
			_service.TransferMessage("task5 started");

			return await _lts.GetUserTasksAsync();
		}
		[HttpGet("task6/{userid}")]
		public async Task<Task6DTO> Task6Async(int userid)
		{
			_service.TransferMessage("task6 started");

			return await _lts.GetUserDefinitionAsync(userid);
		}

		[HttpGet("task7/{projectid}")]
		public async Task<Task7DTO> Task7Async(int projectid)
		{
			_service.TransferMessage("task7 started");

			return await _lts.GetProjectDefinitionAsync(projectid);
		}

		[HttpGet("log")]
		public async Task<List<LogModel>> LogAsync()
		{
			return await _lts.GetLogAsync();
		}
	}
}