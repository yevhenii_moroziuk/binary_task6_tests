﻿using Library.DataAccess;
using Library.DataAccess.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace WebAPI.Test
{
	public class FakeSeed
	{
		public static List<T> Seed<T>(string filename)
		{
			var parsedData = System.IO.File.ReadAllText($"../../../Data/_{filename}.json");
			return JsonConvert.DeserializeObject<List<T>>(parsedData);
		}
	}

	public class SeedService
	{
		private readonly BinaryDataContext _context;

		public SeedService(BinaryDataContext context)
		{
			_context = context;
		}

		public void SeedEntities()
		{
			var users = FakeSeed.Seed<User>("users");
			var projects = FakeSeed.Seed<Project>("projects");
			var tasks = FakeSeed.Seed<Task>("tasks");
			var teams = FakeSeed.Seed<Team>("teams");

			foreach (var item in teams)
			{
				_context.Teams.Add(item);
			}
			foreach (var item in users)
			{
				_context.Users.Add(item);
			}
			foreach (var item in projects)
			{
				_context.Projects.Add(item);
			}
			foreach (var item in tasks)
			{
				_context.Tasks.Add(item);
			}
		}
	}
}
