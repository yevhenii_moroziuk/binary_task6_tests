﻿using Library.DataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using System.Linq;
using Library.Services;
using Microsoft.Extensions.Configuration;
using Moq;
using Library.DataAccess.Models;
using Library.BusinessLogic.DTOs;

namespace WebAPI.Test
{
	[TestFixture]
	class TestGetRequests
	{
		[OneTimeSetUp]
		public void InitInMemoryDb()
		{
			var options = new DbContextOptionsBuilder<BinaryDataContext>()
				.UseInMemoryDatabase(databaseName: "FakeBinaryDB").Options;

			using (var context = new BinaryDataContext(options))
			{
				var seed = new SeedService(context);
				seed.SeedEntities();
				context.SaveChanges();
			}
		}

		[Test]
		public void Test_If_DataSeed_Works()
		{
			var options = new DbContextOptionsBuilder<BinaryDataContext>()
				.UseInMemoryDatabase(databaseName: "FakeBinaryDB").Options;

			using (var context = new BinaryDataContext(options))
			{
				Assert.IsTrue(context.Users.ToList().Count() == 50);
				Assert.IsTrue(context.Tasks.ToList().Count() == 200);
				Assert.IsTrue(context.Teams.ToList().Count() == 10);
				Assert.IsTrue(context.Projects.ToList().Count() == 100);
			}
		}

		#region Test for 1 linq request 
		[Test]
		public void Test_Request_1_When_UserId_2_Then_Data_Should_Be_Dictionary_With_2_elems()
		{
			IConfiguration config = new Mock<IConfiguration>().Object;

			var options = new DbContextOptionsBuilder<BinaryDataContext>()
				.UseInMemoryDatabase(databaseName: "FakeBinaryDB").Options;

			using (var context = new BinaryDataContext(options))
			{
				//Arrange
				IDictionary<string, int> actual;
				IDictionary<string, int> expected = new Dictionary<string, int>
					{
						{"Molestias unde odio et facilis.",1 },
						{"Recusandae dolores explicabo porro molestias.", 6 }
					};

				var linqService = new LinqTaskService(context, config);
				//Act
				actual = linqService.GetTasksCountAsync(2).Result;
				//Assert
				Assert.AreEqual(actual, expected);
			}

		}

		[Test]
		public void Test_If_TestRequest_1_Is_Works_Right()//all other(Test if t2 works right...) will work as this one, so i remove them
		{
			var options = new DbContextOptionsBuilder<BinaryDataContext>()
				.UseInMemoryDatabase(databaseName: "FakeBinaryDB").Options;

			IConfiguration config = new Mock<IConfiguration>().Object;

			using (var context = new BinaryDataContext(options))
			{
				//Arrange
				IDictionary<string, int> actual;
				IDictionary<string, int> expected = new Dictionary<string, int>
				{
					{"Uncorrect text.",1 },
					{"Recusandae dol", 6 }
				};

				var seed = new SeedService(context);

				var linqService = new LinqTaskService(context, config);
				
				//Act
				actual = linqService.GetTasksCountAsync(2).Result;

				//Assert
				Assert.AreNotEqual(actual, expected);
			}

		}
		#endregion

		#region Test for 2 linq request
		[Test]
		public void Test_Request_2_When_UserId_6_Then_Data_Should_Be_1_elem()
		{
			IConfiguration config = new Mock<IConfiguration>().Object;

			var options = new DbContextOptionsBuilder<BinaryDataContext>()
				.UseInMemoryDatabase(databaseName: "FakeBinaryDB").Options;

			using (var context = new BinaryDataContext(options))
			{
				//Arrange
				List<Library.DataAccess.Models.Task> actual;

				var expected = new List<int> { 183 };

				var linqService = new LinqTaskService(context, config);
				//Act
				actual = linqService.GetTasksAsync(6).Result;
				//Assert
				Assert.AreEqual(actual.Select(u => u.Id).ToList(), expected);
			}

		}
		#endregion

		#region Test for 3 linq request 
		[Test]
		public void Test_Request_3_When_UserId_2_Then_Data_Should_Be_List_with_1_string()
		{
			IConfiguration config = new Mock<IConfiguration>().Object;

			var options = new DbContextOptionsBuilder<BinaryDataContext>()
				.UseInMemoryDatabase(databaseName: "FakeBinaryDB").Options;

			using (var context = new BinaryDataContext(options))
			{
				//Arrange
				List<string> actual;
				List<string> expected = new List<string>
				{
					{"Task Id: 138, Task name: Veritatis culpa sunt."}
				};		

				var linqService = new LinqTaskService(context, config);
				
				//Act
				actual = linqService.GetFinishedTasksAsync(2).Result;

				//Assert
				Assert.AreEqual(actual, expected);
			}

		}
		#endregion

		#region Test for 4 linq request 
		[Test]
		public void Test_Request_4_EveryTime_Must_Be_0()
		{
			IConfiguration config = new Mock<IConfiguration>().Object;

			var options = new DbContextOptionsBuilder<BinaryDataContext>()
				.UseInMemoryDatabase(databaseName: "FakeBinaryDB").Options;

			using (var context = new BinaryDataContext(options))
			{
				//Arrange
				IDictionary<string, List<User>> actual;
				IDictionary<string, List<User>> expected = new Dictionary<string, List<User>>{};
				
				var linqService = new LinqTaskService(context, config);
				
				//Act
				actual = linqService.GetTeamDefinitionAsync().Result;

				//Assert
				Assert.AreEqual(actual, expected);
			}

		}
		#endregion

		#region Test for 5 linq request
		[Test]
		public void Test_Request_5_When_Actual_Data_Should_Be_The_Same_As_Expected()
		{
			IConfiguration config = new Mock<IConfiguration>().Object;

			var options = new DbContextOptionsBuilder<BinaryDataContext>()
				.UseInMemoryDatabase(databaseName: "FakeBinaryDB").Options;

			using (var context = new BinaryDataContext(options))
			{
				//Arrange
				IDictionary<User, List<Library.DataAccess.Models.Task>> actual;
				var expectedFirstUserId = 19;
				var expectedLastUserId = 20;
				var expectedCountOfUsers = 50;
				var expectedCountOfTaskOfTheSecondUser = 3;

				var linqService = new LinqTaskService(context, config);
				//Act
				actual = linqService.GetUserTasksAsync().Result;

				var actualFirstUserId = actual.Keys.First().Id;
				var actualLastUserId = actual.Keys.Last().Id; ;
				var actualCountOfUsers = actual.Keys.Count();
				var actualCountOfTaskOfTheLastUser = actual[actual.Keys.Last()].Count();

				//Assert
				Assert.AreEqual(expectedFirstUserId, actualFirstUserId);
				Assert.AreEqual(expectedLastUserId, actualLastUserId);
				Assert.AreEqual(expectedCountOfUsers, actualCountOfUsers);
				Assert.AreEqual(expectedCountOfTaskOfTheSecondUser, actualCountOfTaskOfTheLastUser);
			}

		}
		#endregion

		#region Test for 6 linq request 
		[Test]
		public void Test_Request_6_When_UserId_9()
		{
			IConfiguration config = new Mock<IConfiguration>().Object;

			var options = new DbContextOptionsBuilder<BinaryDataContext>()
				.UseInMemoryDatabase(databaseName: "FakeBinaryDB").Options;

			using (var context = new BinaryDataContext(options))
			{
				//Arrange
				Task6DTO actual;
				 
				var expectedUserId = 9;
				var expectedProjectId = 67;
				var expectedInt1 = 0;
				var expectedInt2 = 4;
				var expectedTaskid = 12;

				var linqService = new LinqTaskService(context, config);

				//Act
				actual = linqService.GetUserDefinitionAsync(9).Result;

				var actualUserId = actual.User.Id;
				var actualProjectId = actual.LastProject.Id;
				var actualInt1 = actual.TaskCount;
				var actualInt2 = actual.UnfinishedTaskCount;
				var actualTaskid = actual.LongestTask.Id;

				//Assert
				Assert.AreEqual(actualUserId, expectedUserId);
				Assert.AreEqual(actualInt1, expectedInt1);
				Assert.AreEqual(actualInt2, expectedInt2);
				Assert.AreEqual(actualTaskid, expectedTaskid);
				Assert.AreEqual(actualProjectId, expectedProjectId);
			}

		}
		#endregion

		#region Test for 7 linq request 
		[Test]
		public void Test_Request_7_When_ProjectId_10()
		{
			IConfiguration config = new Mock<IConfiguration>().Object;

			var options = new DbContextOptionsBuilder<BinaryDataContext>()
				.UseInMemoryDatabase(databaseName: "FakeBinaryDB").Options;

			using (var context = new BinaryDataContext(options))
			{
				//Arrange
				Task7DTO actual;

				var expectedTaskId1 = 16;
				var expectedTaskId2 = 16;
				var expectedProjectId = 10;
				var expectedInt = 7;

				var linqService = new LinqTaskService(context, config);

				//Act
				actual = linqService.GetProjectDefinitionAsync(10).Result;

				var actualTaskId1 = actual.TaskByDescription.Id;
				var actualTaskId2 = actual.TaskByLength.Id;
				var actualProjectId = actual.Project.Id;
				var ActualInt = actual.CountOfUsers;

				//Assert
				Assert.AreEqual(actualTaskId1, expectedTaskId1);
				Assert.AreEqual(actualTaskId2, expectedTaskId2);
				Assert.AreEqual(actualProjectId ,expectedProjectId);
				Assert.AreEqual(ActualInt, expectedInt);
			}

		}
		#endregion
	}
}
